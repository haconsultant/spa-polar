import tableau from 'tableau-api'

const url = 'https://public.tableau.com/views/OfertasAutogeneradas/Dashboard1?:embed=y&:display_count=yes&publish=yes'
const options = {
  'BigClient/Id BigClient': '4',
  hideTabs: true
}

export function renderReport(container) {
  console.log(container)
  var viz = new tableau.Viz(container, url, options)
}
