import request from '@/utils/request'
import store from '@/store'

// Get All Products
export function fetchListProducts() {
  return request({
    url: '/products/bigclients/' + store.getters.id,
    method: 'get'
  })
}
// Get Product
export function fetchProduct(resource) {
  return request({
    url: '/products/' + resource,
    method: 'get'
  })
}

// Get Distribution Enterprise’s Product
export function fetchListProductsAviable() {
  return request({
    url: '/products/distributionenterpriseavailable',
    method: 'get'
  })
}
// Get Clients
export function fetchListClients() {
  return request({
    url: '/bigclients',
    method: 'get'
  })
}

// Get Client’s Product
export function fetchListProductsProviders(resource) {
  return request({
    url: 'products/bigclients/' + resource + '/availableforaffiliation',
    method: 'get'
  })
}

// Create Client
export function createProduct(data) {
  return request({
    url: '/products',
    method: 'post'
  })
}

// Update Client
export function updateProduct(resource, data) {
  return request({
    url: 'products/' + resource,
    method: 'patch',
    data
  })
}
