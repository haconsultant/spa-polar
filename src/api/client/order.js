import request from '@/utils/request'
import store from '@/store'

// Get Client Orders :/selfapprovedorders/bigclients/{BigClientId}
export function fetchListClientOrders(resource) {
  return request({
    url: '/selfapprovedorders/bigclients/' + store.getters.id,
    method: 'get'
  })
}

// Get Client Offer for Review :/selfapprovedorders/bigclients/{SelfApprovedOrderId}
export function fetchClientOrder(resource) {
  return request({
    url: '/selfapprovedorders/bigclients/' + resource,
    method: 'get'
  })
}

