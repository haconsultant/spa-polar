import request from '@/utils/request'
import store from '@/store'

// Get Client Alerts :/bigclientalerts/bigclients/{BigClientId}
export function fetchListClientAlerts(resource) {
  return request({
    url: '/bigclientalerts/bigclients/' + store.getters.id,
    method: 'get'
  })
}

// Get Client Alert for Update :/bigclientalerts/{BigClientAlert}
export function fetchClientAlert(resource) {
  return request({
    url: '/bigclientalerts/' + resource,
    method: 'get'
  })
}

// Create Client Alert /bigclientalerts
export function creatClientAlert(data) {
  return request({
    url: '/bigclientalerts',
    method: 'post',
    data
  })
}

// Update Client Alert :/bigclientalerts/{BigClientAlert}
export function updateClientAlerts(resource, data) {
  return request({
    url: '/bigclientalerts/' + resource,
    method: 'patch',
    data
  })
}

