import request from '@/utils/request'
import store from '@/store'

// Get Client Busnisses Rule /bigclientbusinessrules/bigclients/{BigClientId}
export function fetchListClientBusnisseRules() {
  return request({
    url: '/bigclientbusinessrules/bigclients/' + store.getters.id,
    method: 'get'
  })
}

// Get Client Busnisses Rule for Update /bigclientbusinessrules/{BigClientBusinessRuleId
export function fecthClientBusnissesRule(resource) {
  return request({
    url: '/bigclientbusinessrules/' + resource,
    method: 'get'
  })
}

// Get All Products /products/bigclients/{BigClientId}/availableforbusinessrules
export function fetchListProduct() {
  return request({
    url: '/products/bigclients/' + store.getters.id + '/availableforbusinessrules',
    method: 'get'
  })
}

// Create Client Busnisse Rule /bigclientbusinessrules
export function creatClientBusnisseRule(data) {
  return request({
    url: '/bigclientbusinessrules',
    method: 'post',
    data
  })
}

// Update Client Busnisse Rule /bigclientbusinessrules
export function updateClientBusnisseRule(resource, data) {
  return request({
    url: '/bigclientbusinessrules/' + resource,
    method: 'pacth',
    data
  })
}

