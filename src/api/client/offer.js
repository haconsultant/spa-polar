import request from '@/utils/request'
import store from '@/store'

// Get Client Offers :/selfgeneratedoffers/bigclients/{BigClientId}
export function fetchListClientOffers(resource) {
  return request({
    url: '/selfgeneratedoffers/bigclients/' + store.getters.id,
    method: 'get'
  })
}

// Get Client Offer for Update :/selfgeneratedoffers/{SelfGeneratedOfferId}
export function fetchClientOffer(resource) {
  return request({
    url: '/selfgeneratedoffers/' + resource,
    method: 'get'
  })
}

// Create Client Offer :/selfgeneratedoffers
export function creatClientOffer(data) {
  return request({
    url: '/selfgeneratedoffers',
    method: 'post',
    data
  })
}

// Update Client Offer :/selfgeneratedoffers/{SelfGeneratedOfferId}
export function updateClientOffer(resource, data) {
  return request({
    url: '/selfgeneratedoffers/' + resource,
    method: 'patch',
    data
  })
}
