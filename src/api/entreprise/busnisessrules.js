import request from '@/utils/request'

// Get Entreprise Busnisses Rules: /distributionenterprisebusinessrules
export function fetchListEntrepriseBusnisseRules() {
  return request({
    url: '/distributionenterprisebusinessrules',
    method: 'get'
  })
}

// Get Client Busnisses Rules :/bigclients/availablefordistributionenterprisebusinessrules
export function fetchListClientBusnisseRule() {
  return request({
    url: '/bigclients/availablefordistributionenterprisebusinessrules',
    method: 'get'
  })
}

// Get Entreprise Busnisse Rule for Update :/distributionenterprisebusinessrules/{DistributionEnterpriseBusinessRule}
export function fecthEntreprisesBusnisseRule(resource) {
  return request({
    url: '/distributionenterprisebusinessrules/' + resource,
    method: 'get'
  })
}

// Create Entreprises Busnisse Rule /distributionenterprisebusinessrules
export function creatEntreprisesBusnisseRule(data) {
  return request({
    url: '/distributionenterprisebusinessrules',
    method: 'post',
    data
  })
}

// Update Entreprise Busnisse Rule :/distributionenterprisebusinessrules/{DistributionEnterpriseBusinessRule}
export function updateEntreprisesBusnisseRule(resource, data) {
  return request({
    url: '/distributionenterprisebusinessrules/' + resource,
    method: 'patch',
    data
  })
}

