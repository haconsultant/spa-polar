import request from '@/utils/request'

// Get All Products
export function fetchListProducts() {
  return request({
    url: '/products',
    method: 'get'
  })
}

// Get Distribution Enterprise’s Product :/products/distributionenterpriseavailable
export function fetchListProductsAviable() {
  return request({
    url: '/products/distributionenterpriseavailable',
    method: 'get'
  })
}
// Get Clients
export function fetchListClients() {
  return request({
    url: '/bigclients',
    method: 'get'
  })
}

// Get Client’s Product
export function fetchListProductsProviders(resource) {
  if (resource) {
    return request({
      url: 'products/bigclients/' + resource + '/availableforaffiliation',
      method: 'get'
    })
  }
}
// Get Prduct for Update :/products/{ProductId}
export function fetchProduct(resource) {
  return request({
    url: '/products/' + resource,
    method: 'get'
  })
}

// Create Product
export function creatProduct(data) {
  return request({
    url: '/products',
    method: 'post',
    data
  })
}
// Udate Product :/products/{ProductId}
export function updateProduct(resource, data) {
  return request({
    url: '/products/' + resource,
    method: 'patch',
    data
  })
}
