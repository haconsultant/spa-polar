import request from '@/utils/request'

// Get Entrerprise Orders :/selfapprovedorders
export function fetchListEntrerpriseOrders(resource) {
  return request({
    url: '/selfapprovedorders',
    method: 'get'
  })
}
// Get Entrerprise Order for Update :/selfapprovedorders/{SelfApprovedOrder}
export function fetchEntrerpriseOrder(resource) {
  return request({
    url: '/selfapprovedorders/' + resource,
    method: 'get'
  })
}

// Update Entrerprise Orders for Update :/selfapprovedorders/{SelfApprovedOrder}
export function updateEntrerpriseOrders(data, resource) {
  return request({
    url: '/selfapprovedorders/' + resource,
    method: 'patch',
    data
  })
}

// Orden Approve :/selfapprovedorders/approve 
export function approveOrden(data) {
  return request({
    url: '/selfapprovedorders/approve',
    method: 'post',
    data
  })
}

// Orden Disapprove :/selfapprovedorders/disapprove 
export function disapproveOrden(data) {
  return request({
    url: '/selfapprovedorders/disapprove',
    method: 'post',
    data
  })
}
