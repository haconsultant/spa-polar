import request from '@/utils/request'

// Get Entreprise Offers :/selfgeneratedoffers
export function fetchListEntrepriseOffers() {
  return request({
    url: '/selfgeneratedoffers',
    method: 'get'
  })
}

// Get Entreprise Offer for Approvement :/selfgeneratedoffers/{SelfGeneratedOfferId}
export function fetchEntrepriseOffer(resource) {
  return request({
    url: '/selfgeneratedoffers/' + resource,
    method: 'get'
  })
}

// Create Entreprise Offer :/selfgeneratedoffers
export function creatEntrepriseOffer(data) {
  return request({
    url: '/selfgeneratedoffers',
    method: 'post',
    data
  })
}

// Approvement Entreprise Offer :/selfgeneratedoffers/{SelfGeneratedOfferId}
export function approveEntrepriseOffer(resource, data) {
  return request({
    url: '/selfgeneratedoffers/' + resource,
    method: 'post',
    data
  })
}
