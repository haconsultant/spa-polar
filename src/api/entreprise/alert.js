import request from '@/utils/request'

// Get Entreprise Alerts :/distributionenterprisealerts
export function fetchListEntrepriseAlerts() {
  return request({
    url: '/distributionenterprisealerts',
    method: 'get'
  })
}

// Get Entreprise Alert for Update :/distributionenterprisealerts/{DistributionEnterpriseAlertId}
export function getEntrepriseAlerts(resource) {
  return request({
    url: '/distributionenterprisealerts/' + resource,
    method: 'get'
  })
}

// Create Entreprise Alert /distributionenterprisealerts
export function creatEntrepriseAlert(data) {
  return request({
    url: '/distributionenterprisealerts',
    method: 'post',
    data
  })
}

// Update Entreprise Alert :/distributionenterprisealerts/{DistributionEnterpriseAlertId}
export function updateEntrepriseAlerts(resource, data) {
  return request({
    url: '/distributionenterprisealerts/' + resource,
    method: 'patch',
    data
  })
}
