import request from '@/utils/request'

// Get All Clients
export function fetchListClients() {
  return request({
    url: '/bigclients',
    method: 'get'
  })
}

// Get Aviable Clients :
export function fetchListClientsAviable() {
  return request({
    url: '/bigclients/distributionenterpriseavailable',
    method: 'get'
  })
}

// Get Clients Providers :/bigclients/{BigClientId}/bigclientavailableproviders
export function fetchListClientsProviders(resource) {
  if (resource) {
    return request({
      url: 'bigclients/' + resource + '/bigclientavailableproviders',
      method: 'get'
    })
  }
}
// Get Client for Update
export function creatClient(data) {
  return request({
    url: '/bigclients',
    method: 'post',
    data
  })
}

// Get Client for Update
export function fetchClient(resource) {
  return request({
    url: '/bigclients/' + resource,
    method: 'get'
  })
}
creatClient
// Update Client
export function updateClient(resource, data) {
  return request({
    url: '/bigclients/' + resource,
    method: 'patch',
    data
  })
}
