import Vue from 'vue'
import Router from 'vue-router'
const _import = require('./_import_' + process.env.NODE_ENV)
// in development-env not use lazy-loading, because lazy-loading too many pages will cause webpack hot update too slow. so only in production use lazy-loading;
// detail: https://panjiachen.github.io/vue-element-admin-site/#/lazy-loading

Vue.use(Router)
/* Layout */
import Layout from '../views/layout/Layout'

/** note: submenu only apppear when children.length>=1
*   detail see  https://panjiachen.github.io/vue-element-admin-site/#/router-and-nav?id=sidebar
**/

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirct in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    roles: ['admin','editor']     will control the page roles (you can set multiple roles)
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar,
    noCache: true                if true ,the page will no be cached(default is false)
  }
**/
export const constantRouterMap = [
  { path: '/login', component: _import('login/index'), hidden: true },
  { path: '/authredirect', component: _import('login/authredirect'), hidden: true },
  { path: '/404', component: _import('errorPage/404'), hidden: true },
  { path: '/401', component: _import('errorPage/401'), hidden: true },
  {
    path: '',
    component: Layout,
    redirect: 'dashboard',
    children: [{
      path: 'dashboard',
      component: _import('dashboard/index'),
      name: 'dashboard',
      meta: { title: 'dashboard', icon: 'dashboard', noCache: true }
    }]
  }
]

export default new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})

export const asyncRouterMapDistributor = [
  {
    path: '/entreprise/modules',
    component: Layout,
    redirect: 'noredirect',
    name: 'entreprise',
    meta: {
      title: 'Distribution Enterprise Options',
      icon: 'component'
    },
    children: [
      { path: 'clients', component: _import('entreprise/modules/clients/index'), name: 'distributior_clients', meta: { title: 'Client', noCache: true }},
      { path: 'products', component: _import('entreprise/modules/products/index'), name: 'distributior_products', meta: { title: 'Product', noCache: true }},
      { path: 'businessrules', component: _import('entreprise/modules/businessrules/index'), name: 'distributior_businessrules', meta: { title: 'Business Rule', noCache: true }},
      { path: 'alert', component: _import('entreprise/modules/alert/index'), name: 'distributior_alert', meta: { title: 'Alert', noCache: true }},
      { path: 'offers', component: _import('entreprise/modules/offers/index'), name: 'distributior_offers', meta: { title: 'Offer', noCache: true }},
      { path: 'orders', component: _import('entreprise/modules/orders/index'), name: 'distributior_orders', meta: { title: 'Order', noCache: true }},
     // { path: 'bi/reports', component: Layout, name: 'bi', meta: { title: 'BI' }}
    ]
  },
  { path: '*', redirect: '/404', hidden: true }
]

// / Client Menus
export const asyncRouterMapClient = [
  {
    path: '/client/modules',
    component: Layout,
    redirect: 'noredirect',
    name: 'client',
    meta: {
      title: 'Client Options',
      icon: 'component',
      roles: ['B']
    },
    children: [
      { path: 'products', component: _import('client/modules/products/index'), name: 'client_products', meta: { title: 'Product' }},
      { path: 'businessrules', component: _import('client/modules/businessrules/index'), name: 'client_businessrules', meta: { title: 'Business Rule' }},
      { path: 'alert', component: _import('client/modules/alert/index'), name: 'client_alert', meta: { title: 'Alert' }},
      { path: 'offers', component: _import('client/modules/offers/index'), name: 'client_offers', meta: { title: 'Offer' }},
      { path: 'orders', component: _import('client/modules/orders/index'), name: 'client_orders', meta: { title: 'Order' }},
      { path: 'bi', component: _import('client/modules/bi/index'), name: 'client_bi', meta: { title: 'BI' }}
    ]
  },
  { path: '*', redirect: '/404', hidden: true }
]
